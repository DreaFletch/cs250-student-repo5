#include "DataEntry.hpp"

void DataEntry::Output( ofstream& output )
{
    for ( map<string, string>::iterator it = fields.begin();
            it != fields.end(); it++ )
    {
        output << left << setw( 30 ) << it->second;
    }
    output << endl;
}

void ReadData( vector<DataEntry>& data, const string& filename )
{
    ifstream input( filename );

    vector<string> headerItems;

    int field = 0;

    string line;
    bool header = true;
    bool skippedGeoComma = false;
    while ( getline( input, line ) )
    {
        DataEntry entry;

        int columnBegin = 0;
        field = 0;
        skippedGeoComma = false;

        for ( unsigned int i = 0; i < line.size(); i++ )
        {
            //cout << line[i];
            if ( line[i] == ',' )
            {
                int length = i - columnBegin;
                string substring = line.substr( columnBegin, length );

                if ( header )
                {
                    headerItems.push_back( substring );
                }
                else
                {
					string fieldKey = "unknown";
					if (field < headerItems.size())
					{
						fieldKey = headerItems[field];
					}
					entry.fields[fieldKey] = substring;
                }

                columnBegin = i+1;

                if ( header == false && skippedGeoComma == false && headerItems[ field ] == "GeoLocation" )
                {
                    skippedGeoComma = true;
                    // Ignore this comma.
                    continue;
                }
                else
                {
                    field++;
                }
            }
        }

        if ( header )
        {
            header = false;
        }
        else
        {
            data.push_back( entry );
        }
    }

    input.close();
}
