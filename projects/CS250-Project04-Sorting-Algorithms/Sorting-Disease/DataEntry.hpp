#ifndef _DATA_ENTRY_HPP
#define _DATA_ENTRY_HPP

#include <string>
#include <fstream>
#include <iomanip>
#include <vector>
#include <map>
using namespace std;

struct DataEntry
{
    map<string, string> fields;
    void Output( ofstream& output );
};

void ReadData( vector<DataEntry>& data, const string& filename );

#endif
