#ifndef _SORT_FUNCTIONS_HPP
#define _SORT_FUNCTIONS_HPP

#include <iostream>
#include <vector>
using namespace std;

#include "DataEntry.hpp"

void SelectionSort( vector<DataEntry>& data, const string& onKey );

#endif
